#!/usr/bin/python
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import sys

import Adafruit_DHT
import subprocess
import urllib
#import urllib3
import ssl
import pycurl, json
from random import randint

# import Rpi.GPIO as GPIO

# GPIO.setmode(GPIO.BOARD)

# GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# Parse command line parameters.
sensor_args = { '11': Adafruit_DHT.DHT11,
				'22': Adafruit_DHT.DHT22,
				'2302': Adafruit_DHT.AM2302 }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
	sensor = sensor_args[sys.argv[1]]
	pin = sys.argv[2]
else:
	print 'usage: sudo ./Adafruit_DHT.py [11|22|2302] GPIOpin#'
	print 'example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO #4'
	sys.exit(1)

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

# Note that sometimes you won't get a reading and
# the results will be null (because Linux can't
# guarantee the timing of calls to read the sensor).  
# If this happens try again!
if humidity is not None and temperature is not None:
	print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)
	#cmd='''curl -k -H 'appKey:4713b8e4-5ef0-44c9-bc22-14760d672d70' -H 'Content-Type:application/json' -X 'PUT' --data '{"I1":77,"I2":52}' https://i3liot4.cloudapp.net:8443/Thingworx/Things/GenTestThing/Services/SetPropValues'''
	#args = cmd.split()
	#process= subprocess.Popen(args,shell=False, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	#stdout,stderr = process.communicate()	
	#print 'Done. Try again!'
	#print stderr
	#print stdout



	url='https://i3liot3.cloudapp.net:8443/Thingworx/Things/Cubicle%201/Services/setPropValues'
	while True:
		humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
		print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)

		lightIntensity = (randint(230,1200))
		powerConsumption = (randint(40,150))
		isCubicalOccupied = bool(randint(0,1))
		print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity), "lightIntensity:",lightIntensity, "powerConsumption:",powerConsumption, "isCubicalOccupied:",isCubicalOccupied

		data = json.dumps({"temperature":temperature,"humidityLevel":humidity, "isCubicalOccupied":isCubicalOccupied, "powerConsumption":powerConsumption,"lightIntensity":lightIntensity})

		c = pycurl.Curl() 
		c.setopt(pycurl.URL, url)
		c.setopt(pycurl.HTTPHEADER, ['appKey:9b6d9e76-7fd6-4ed8-9b78-47af166ec977', 'Content-Type:application/json']) 
		c.setopt(pycurl.POST, 1)
		c.setopt(pycurl.POSTFIELDS, data) 
		c.setopt(pycurl.SSL_VERIFYPEER,0)
		c.setopt(pycurl.SSL_VERIFYHOST,0)
		#c.setopt(pycurl.SSL_CIPHER_LIST,'rsa_rc4_128_sha')
		c.perform()

	
else:
	print 'Failed to get reading. Try again!'
